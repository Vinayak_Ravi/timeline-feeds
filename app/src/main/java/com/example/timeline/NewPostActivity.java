package com.example.timeline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import id.zelory.compressor.Compressor;


public class NewPostActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView newpostimage;
    private TextInputEditText post_desc;
    private Uri postimageUri;
    private Button post_btn;
    private ProgressBar newpostprogreebar;
    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private String current_user_id;

    private Bitmap compressedImageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        Calendar calendar = Calendar.getInstance();
        final String currentDate = DateFormat.getDateInstance().format(calendar.getTime());


        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        current_user_id = firebaseAuth.getCurrentUser().getUid();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add New Post");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        newpostimage = (ImageView) findViewById(R.id.new_post_image);
        post_desc = (TextInputEditText) findViewById(R.id.desc);
        post_btn = (Button) findViewById(R.id.add_post);
        newpostprogreebar = findViewById(R.id.newpostprogress);


        newpostimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)
                        .setMinCropResultSize(512, 512)
                        .start(NewPostActivity.this);
            }
        });

        post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String desc = post_desc.getText().toString();

                if (!TextUtils.isEmpty(desc) && postimageUri != null) {

                    newpostprogreebar.setVisibility(View.VISIBLE);

                    final String randomName = UUID.randomUUID().toString();
                    final StorageReference filepath = storageReference.child("post_images").child(randomName + ".jpg");

                    final UploadTask uploadTask = filepath.putFile(postimageUri);

                    uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                Toast.makeText(NewPostActivity.this, "ok", Toast.LENGTH_SHORT).show();

                                throw task.getException();
                            }

                            // Continue with the task to get the download URL
                            return filepath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull final Task<Uri> task) {

                            File newImageFile = new File(postimageUri.getPath());

                            try {
                                compressedImageFile = new Compressor(NewPostActivity.this)
                                        .setMaxHeight(200)
                                        .setMaxWidth(200)
                                        .setQuality(10)
                                        .compressToBitmap(newImageFile);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            compressedImageFile.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] thumb_data = baos.toByteArray();
                            UploadTask thumbFilePath = storageReference.child("post_images/thumbs").child(randomName + "jpg").putBytes(thumb_data);

                            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    if (task.isSuccessful()) {
                                        Uri downloadUri = task.getResult();
                                        Map<String, Object> userMap = new HashMap<>();
                                        userMap.put("image_url", downloadUri.toString());
                                        userMap.put("desc", desc);
                                        userMap.put("user_id", current_user_id);
                                      userMap.put("timestamp",FieldValue.serverTimestamp());
                                        firebaseFirestore.collection("Posts").add(userMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentReference> task) {

                                                if (task.isSuccessful()) {

                                                    Toast.makeText(NewPostActivity.this, "Post Added", Toast.LENGTH_SHORT).show();
                                                    Intent i = new Intent(NewPostActivity.this, LandingPage.class);
                                                    startActivity(i);
                                                    finish();
                                                } else {

                                                }

                                                newpostprogreebar.setVisibility(View.INVISIBLE);
                                            }
                                        });

                                    } else {

                                        newpostprogreebar.setVisibility(View.INVISIBLE);

                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                    //Error Handling
                                }
                            });


                        }


                        // StorageReference filePath = storageReference.child("post_images").child(randomName + ".jpg");
                        // filepath.putFile(postimageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        //  @Override
                       /* public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                            if(task.isSuccessful()){

                                String downloadUri = task.getResult().getUploadSessionUri().toString();

                                Map<String,Object> postMap = new HashMap<>();
                                postMap.put("image-url",downloadUri);
                                postMap.put("desc",desc);
                                postMap.put("user_id",current_user_id);
                                postMap.put("timestamp",FieldValue.serverTimestamp());

                                firebaseFirestore.collection("Posts").add(postMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentReference> task) {

                                        if (task.isSuccessful()) {

                                            Toast.makeText(NewPostActivity.this,"Post Added",Toast.LENGTH_SHORT).show();
                                            Intent i = new Intent(NewPostActivity.this,LandingPage.class);
                                            startActivity(i);
                                            finish();
                                        } else {

                                        }

                                        newpostprogreebar.setVisibility(View.INVISIBLE);
                                    }
                                });

                            } else{

                                newpostprogreebar.setVisibility(View.INVISIBLE);

                            }
                        } */
                    });


                }
            }


        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                postimageUri = result.getUri();

                newpostimage.setImageURI(postimageUri);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}


//    public static String random() {
//        Random generator = new Random();
//        StringBuilder randomStringBuilder = new StringBuilder();
//        int randomLength = generator.nextInt(MAX_LENGTH);
//        char tempChar;
//        for (int i = 0; i < randomLength; i++){
//            tempChar = (char) (generator.nextInt(96) + 32);
//            randomStringBuilder.append(tempChar);
//        }
//        return randomStringBuilder.toString();
//    }
//}
