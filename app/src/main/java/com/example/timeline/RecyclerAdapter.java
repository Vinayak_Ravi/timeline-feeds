package com.example.timeline;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Nullable;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public  List<BlogPost> postList;
    public Context context;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;


    public RecyclerAdapter(List<BlogPost> postList){

        this.postList = postList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);

       context=parent.getContext();
       firebaseAuth = firebaseAuth.getInstance();
       firebaseFirestore = FirebaseFirestore.getInstance();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.setIsRecyclable(false);
        final String postId = postList.get(position).PostId;

        final String current_user_id = firebaseAuth.getCurrentUser().getUid();


        String desc_data = postList.get(position).getDesc();
        holder.setdescText(desc_data);

        String image_url =postList.get(position).getImage_url();
        holder.setImage(image_url);

        final String user_id = postList.get(position).getUser_id();

        if(user_id.equals(current_user_id)){

            holder.post_Delete.setEnabled(true);
            holder.post_Delete.setVisibility(View.VISIBLE);
        }
        firebaseFirestore.collection("Users").document(user_id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {

                    String userName = task.getResult().getString("name");
                    String userImage = task.getResult().getString("image");

                    holder.setUserData(userName,userImage);
                }else{

                }
            }
        });

          long milliseconds = postList.get(position).getTimestamp().getTime();
            String datestring = new SimpleDateFormat("MM/dd/yyyy").format(new Date(milliseconds));
            holder.setTime(datestring);


            //get Count

        firebaseFirestore.collection("Posts/" + postId + "/Likes").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                Log.e("MyTag", "Firebase exception", e);

                if(!queryDocumentSnapshots.isEmpty()){

                    int count = queryDocumentSnapshots.size();

                    holder.updateLikesCount(count);




                }else {

                    holder.updateLikesCount(0);
                }
            }
        });

                //get Likes

                firebaseFirestore.collection("Posts/" + postId + "/Likes").document(current_user_id).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {

                        if (documentSnapshot.exists()) {

                            holder.like_Image.setImageDrawable(context.getDrawable(R.drawable.full_like));
                        } else {
                            holder.like_Image.setImageDrawable(context.getDrawable(R.drawable.like));

                        }
                    }
                });






        //Like Feautures

        holder.like_count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                firebaseFirestore.collection("Posts/" + postId + "/Likes").document(current_user_id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {


                        firebaseFirestore.collection("Posts/" + user_id + "/Notification").document(current_user_id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {


                            }
                        });

                        if(!task.getResult().exists()){
                            Map<String,Object> likeMap = new HashMap<>();
                            likeMap.put("timestamp", FieldValue.serverTimestamp());
                            firebaseFirestore.collection("Posts/" + postId + "/Likes").document(current_user_id).set(likeMap);
                        }else {

                            firebaseFirestore.collection("Posts/" + postId + "/Likes").document(current_user_id).delete();
                        }
                    }
                });



             }
        });

        //Comment

        holder.comment_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent commentIntent = new Intent(context,CommentActivity.class);
                commentIntent.putExtra("blog_post_id",postId);
                context.startActivity(commentIntent);
            }
        });


        holder.post_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder dialog;
                dialog = new AlertDialog.Builder(context);
                dialog.setCancelable(false);
                dialog.setTitle("Delete Post");
                dialog.setMessage("Are you sure you want to delete this Post?" );
                dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //Action for "Delete".

                        firebaseFirestore.collection("Posts").document(postId).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                postList.remove(position);

                            }
                        });
                    }
                })
                        .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Action for "Cancel".
                            }
                        });

                final AlertDialog alert = dialog.create();
                alert.show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        private View mView;
        private TextView descView;
        private ImageView ImageView;
        private TextView upload_date;
        private TextView  UserName;
        private CircleImageView Userimage;
        public  ImageView like_Image;
        public TextView like_count;
        public ImageView comment_Image;
        public ImageButton post_Delete;
        public  TextView comment_text;
        public Menu search;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mView=itemView;

            like_Image = mView.findViewById(R.id.blog_like_btn);
            like_count = mView.findViewById(R.id.blog_like_count);
            comment_Image = mView.findViewById(R.id.blog_comment_icon);
            post_Delete = mView.findViewById(R.id.post_delete);
            comment_text = mView.findViewById(R.id.blog_comment_count);
            search = mView.findViewById(R.id.search);

//            ((View) search).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    firebaseUserSearch();
//                }
//
//                private void firebaseUserSearch() {
//
//                    FirebaseRecyclerAdapter<User,ViewHolder> firebaseRecyclerAdapter  = new FirebaseRecyclerAdapter<User, ViewHolder>(User.class,
//                            R.layout.list_item,
//                            ViewHolder.class,
//                            firebaseFirestore) {
//                        @Override
//                        protected void onBindViewHolder(@NonNull ViewHolder viewHolder, int i, @NonNull User user) {
//
//                        }
//
//                        @NonNull
//                        @Override
//                        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//                            return null;
//                        }
//                    };
//                }
//            });




        }
        public void setdescText(String desctext){

            descView = mView.findViewById(R.id.blog_desc);
            descView.setText(desctext);
        }

        public  void setImage(String downloadUri){

            ImageView  = mView.findViewById(R.id.blog_image);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.placeholder);
            Glide.with(context).applyDefaultRequestOptions(requestOptions).load(downloadUri).into(ImageView);

        }

        public void setTime(String date){
            upload_date = mView.findViewById(R.id.blog_date);
            upload_date .setText(date);

        }
        public void setUserData(String name,String image){

            Userimage = mView.findViewById(R.id.blog_user_image);
            UserName = mView.findViewById(R.id.blog_user_name);

            UserName.setText(name);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.no_pro);

            Glide.with(context).applyDefaultRequestOptions(requestOptions).load(image).into(Userimage);


        }

        public  void updateLikesCount(int count){

            like_count = mView.findViewById(R.id.blog_like_count);
            like_count.setText(count + " Likes");

        }




    }



}
