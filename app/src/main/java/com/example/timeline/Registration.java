package com.example.timeline;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Registration extends AppCompatActivity {

    private TextInputEditText signup_Mail,signup_pass,signup_Cpass;
    private TextView login_back;
    private Button signup_Button;
    private ProgressBar progressBar;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        auth=FirebaseAuth.getInstance();
        signup_Mail = (TextInputEditText)findViewById(R.id.s_Mail);
        signup_pass =(TextInputEditText)findViewById(R.id.s_Password);
        signup_Cpass=(TextInputEditText)findViewById(R.id.con_Pass);
        login_back = (TextView)findViewById(R.id.already_acc);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);


        String text = "Already have an Account? Login";
        SpannableString spannableString = new SpannableString(text);

        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.BLACK);

        spannableString.setSpan(colorSpan,24,30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        login_back.setText(spannableString);


        signup_Button=(Button) findViewById(R.id.signup_But);


        login_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Registration.this,MainActivity.class);
                startActivity(i);
            }
        });

        signup_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(signup_Mail.length()==0){
                    signup_Mail.setError("Mail Required");
                }else if (signup_pass.length()==0){
                    signup_pass.setError("Password Required");
                }else if(signup_Cpass.length()==0){
                    signup_Cpass.setError("Confirm Password Required");
                }

                String email = signup_Mail.getText().toString();
                String pass = signup_pass.getText().toString();
                String cpass = signup_Cpass.getText().toString();

                if(!TextUtils.isEmpty(email)&& !TextUtils.isEmpty(pass)&&!TextUtils.isEmpty(cpass)){
                    if(!pass.equals(cpass)){
                       Toast.makeText(Registration.this,"Confrim Password does not Match",Toast.LENGTH_LONG).show();
                    }
                    if(pass.equals(cpass)){
                        progressBar.setVisibility(View.VISIBLE);

                        auth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if(task.isSuccessful()){

                                    Intent i = new Intent(Registration.this, Setup.class);
                                    startActivity(i);
                                    finish();
                                   // sendToMain();

                                }else {

                                    String errorMessage = task.getException().getMessage();
                                    Toast.makeText(Registration.this,"Error: " +errorMessage,Toast.LENGTH_SHORT).show();

                                }
                                progressBar.setVisibility(View.INVISIBLE);

                            }
                        });
                    }else{

                       // Toast.makeText(Registration.this,"Confrim Paass",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = auth.getCurrentUser();
        if(currentUser != null ){
            sendToMain();
        }
    }

    private void sendToMain() {

        Intent i = new Intent(Registration.this,LandingPage.class);
        startActivity(i);
        finish();

    }
}
