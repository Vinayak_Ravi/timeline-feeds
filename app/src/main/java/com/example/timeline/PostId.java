package com.example.timeline;

import com.google.firebase.firestore.Exclude;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class PostId {

    @Exclude
    public String PostId;

        public <T extends PostId> T withId (final String id){
            this.PostId=id;
            return (T) this;
        }
    }

