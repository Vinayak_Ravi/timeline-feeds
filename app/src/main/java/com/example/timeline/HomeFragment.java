package com.example.timeline;


import android.nfc.TagLostException;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import io.opencensus.tags.Tag;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private RecyclerView list_view;
    private List<BlogPost> postList;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;

    private RecyclerAdapter recyclerAdapter;

    private static final String TAG = "HOME FRAGMENT";


    private DocumentSnapshot  lastVisible;

    private Boolean isFirstPageFirstLoad = true;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        postList = new ArrayList<>();
        list_view = view.findViewById(R.id.post_view);

        firebaseAuth = FirebaseAuth.getInstance();


        recyclerAdapter = new RecyclerAdapter(postList);
        list_view.setLayoutManager(new LinearLayoutManager(container.getContext()));
        list_view.setAdapter(recyclerAdapter);
        list_view.setHasFixedSize(true);

        if(firebaseAuth.getCurrentUser()!=null) {
            firebaseFirestore = FirebaseFirestore.getInstance();

            list_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    Boolean reachedBottom = !recyclerView.canScrollVertically(1);

                    if(reachedBottom){

//                        String desc = lastVisible.getString("desc");
//                        Toast.makeText(container.getContext(),"Reached:" +desc,Toast.LENGTH_SHORT).show();
                        loadMorePost();

                    }
                }
            });


                Query firstQuery = firebaseFirestore.collection("Posts").orderBy("timestamp", Query.Direction.DESCENDING).limit(3);
                firstQuery.addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                    if (e!=null){
//                        Log.d(TAG,"Error:"+e.getMessage());
//                    }else {
                        if(!queryDocumentSnapshots.isEmpty()){

                            if (isFirstPageFirstLoad) {


                                //pageniation
                                lastVisible = queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size() - 1);
                                postList.clear();
                                //
                            }

                            for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {


                                if (doc.getType() == DocumentChange.Type.ADDED) {

                                    String postId = doc.getDocument().getId();

                                    BlogPost blogPost = doc.getDocument().toObject(BlogPost.class).withId(postId);

                                    String postUserId = doc.getDocument().getString("user_id");


                                    if (isFirstPageFirstLoad) {
                                        postList.add(blogPost);

                                    } else {

                                        postList.add(0, blogPost);
                                    }

                                    recyclerAdapter.notifyDataSetChanged();


                                }
                            }
                            isFirstPageFirstLoad = false;


                        }

                    }
                    // }
                });
            }





        return view;
    }



    public void loadMorePost(){
        if (firebaseAuth.getCurrentUser()!=null) {


            Query nextQuery = firebaseFirestore.collection("Posts")
                    .orderBy("timestamp", Query.Direction.DESCENDING)
                    .startAfter(lastVisible)
                    .limit(3);
            nextQuery.addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                    if (!queryDocumentSnapshots.isEmpty()) {
                        //pageniation
                        lastVisible = queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size() - 1);
                        //
                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {


                            if (doc.getType() == DocumentChange.Type.ADDED) {
                                String postId = doc.getDocument().getId();

                                BlogPost blogPost = doc.getDocument().toObject(BlogPost.class).withId(postId);
                                postList.add(blogPost);
                                recyclerAdapter.notifyDataSetChanged();


                            }
                        }


                    }
                }
            });
        }

    }

}
