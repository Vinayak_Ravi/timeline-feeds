package com.example.timeline;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.bumptech.glide.load.model.Model;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;


public class LandingPage extends AppCompatActivity {

    private Toolbar toolbar;

    private FirebaseAuth mAuth;
    private FirebaseFirestore firebaseFirestore;

    private FloatingActionButton addPostBtn;

    private String current_user_id;

    private BottomNavigationView mainBottomNav;

    private HomeFragment homeFragment;
    private NotificationFragment notificationFragment;
    private AccountFragment accountFragment;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        mAuth = FirebaseAuth.getInstance();

        firebaseFirestore  = FirebaseFirestore.getInstance();


        toolbar = (Toolbar) findViewById(R.id.mToolbar);
        setSupportActionBar(toolbar);

       // getSupportActionBar().setTitle("Timeline");

        if(mAuth.getCurrentUser() !=null) {
            mainBottomNav = findViewById(R.id.bottom_bar);

            //Fragments
            homeFragment = new HomeFragment();
            notificationFragment = new NotificationFragment();
            accountFragment = new AccountFragment();
            replacefragment(homeFragment);

            mainBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {

                        case R.id.home_btn:
                            replacefragment(homeFragment);

                            return true;

                        case R.id.notification:
                            replacefragment(notificationFragment);
                            return true;

                        case R.id.account:
                            replacefragment(accountFragment);
                            return true;

                        default:
                            return false;
                    }


                }
            });


            addPostBtn = (FloatingActionButton) findViewById(R.id.add_Photo);
            addPostBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent newPost = new Intent(LandingPage.this, NewPostActivity.class);
                    startActivity(newPost);

                }
            });
       }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        //Search View
        MenuItem item = menu.findItem(R.id.search);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//
//               searchData(s);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                return false;
//            }
//        });


        return true;
    }

    private void searchData(String s) {

        firebaseFirestore.collection("Users").whereEqualTo("search",s.toLowerCase())
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                for(DocumentSnapshot doc:task.getResult()){

                    User user = new User(doc.getString("id"),
                            doc.getString("name"));


                }



            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.setting:
                Intent i = new Intent(LandingPage.this, Setup.class);
                startActivity(i);
              return true;

            case R.id.log_out:
                logout();
                return true;

                default:
                    return false;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();


        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null){
            sendTologin();

        }else {
            current_user_id = mAuth.getCurrentUser().getUid();
            firebaseFirestore.collection("Users").document(current_user_id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    if(task.isSuccessful()){

                        if(!task.getResult().exists()){

                            Intent setupIntent =new Intent(LandingPage.this,Setup.class);
                            startActivity(setupIntent);
                            finish();;
                        }
                    }else {
                        String errorMessage = task.getException().getMessage();
                        Toast.makeText(LandingPage.this,"Error:" +errorMessage,Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void sendTologin() {

        Intent i = new Intent(LandingPage.this,MainActivity.class);
        startActivity(i);
        finish();
    }

    private void sendToStart() {

        Intent i = new Intent(LandingPage.this,MainActivity.class);
        startActivity(i);
        finish();
    }



    private void logout() {

        mAuth.signOut();
        sendTologin();
    }

    private void replacefragment(Fragment fragment){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container,fragment);
        fragmentTransaction.commit();
    }


}



