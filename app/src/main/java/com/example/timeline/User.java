package com.example.timeline;

public class User {

    public String image,mname;

    public User(String id){}

    public User(String image, String mname) {
        this.image = image;
        this.mname = mname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }
}
