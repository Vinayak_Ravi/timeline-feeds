package com.example.timeline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;


public class Setup extends AppCompatActivity {

    private Toolbar mToolbar;
    private CircleImageView setupImage;
    private Uri mainImageURI = null;
    private  boolean isChanged = false;
    private String user_id;

    private TextInputEditText pro_Name;
    private Button account_Update;
    private ProgressBar progressBar;

    private StorageReference mStorageRef;
    private FirebaseAuth firebaseAuth;

    private FirebaseFirestore firebaseFirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        firebaseAuth = FirebaseAuth.getInstance();
        user_id = firebaseAuth.getCurrentUser().getUid();
        firebaseFirestore = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();




        pro_Name = (TextInputEditText) findViewById(R.id.user_Name);
        account_Update = (Button) findViewById(R.id.update);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);

        progressBar.setVisibility(View.VISIBLE);
        account_Update.setEnabled(false);

        firebaseFirestore.collection("Users").document(user_id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {

                    if (task.getResult().exists()) {

                        String name = task.getResult().getString("name");
                        String image = task.getResult().getString("image");

                        mainImageURI = Uri.parse(image);

                        pro_Name.setText(name);

                        RequestOptions placeHolder = new RequestOptions();
                        placeHolder.placeholder(R.drawable.no_pro);
                        Glide.with(Setup.this).setDefaultRequestOptions(placeHolder).load(image).into(setupImage);
                       // Toast.makeText(Setup.this, "Data Exixts", Toast.LENGTH_SHORT).show();


                    } else {
                       // Toast.makeText(Setup.this, "No data", Toast.LENGTH_SHORT).show();

                    }

                } else {


                    String error = task.getException().getMessage();
                    Toast.makeText(Setup.this, "Firebaseretrieve Error" + error, Toast.LENGTH_SHORT).show();
                }

                progressBar.setVisibility(View.INVISIBLE);
                account_Update.setEnabled(true);
            }
        });

        account_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (setupImage==null){
                    RequestOptions placeHolder = new RequestOptions();
                    placeHolder.placeholder(R.drawable.no_pro);
                    Glide.with(Setup.this).setDefaultRequestOptions(placeHolder);
                }

                final String user_name = pro_Name.getText().toString();

                progressBar.setVisibility(View.VISIBLE);

                if(isChanged) {

                    if (!TextUtils.isEmpty(user_name) && mainImageURI != null) {


                        user_id = firebaseAuth.getCurrentUser().getUid();
//                        StorageReference image_path = mStorageRef.child("profile_images").child(user_id + ".jpg");

                        final StorageReference image_path = mStorageRef.child("profile_images").child(user_id + ".jpg");
                        UploadTask uploadTask = image_path.putFile(mainImageURI);

                        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {
                                    throw task.getException();
                                }

                                // Continue with the task to get the download URL
                                return image_path.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    Uri downloadUri = task.getResult();
                                    Map<String, String> userMap = new HashMap<>();
                                    userMap.put("name", user_name);
                                    userMap.put("search",user_name.toLowerCase());
                                    userMap.put("image", downloadUri.toString());
                                    firebaseFirestore.collection("Users").document(user_id).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()) {

                                                Toast.makeText(Setup.this, "User Setting are Updated", Toast.LENGTH_SHORT).show();

                                                Intent i = new Intent(Setup.this, LandingPage.class);
                                                startActivity(i);
                                                finish();
                                            } else {

                                                String error = task.getException().getMessage();
                                                Toast.makeText(Setup.this, "FirebaseStore Error" + error, Toast.LENGTH_SHORT).show();
                                            }

                                            progressBar.setVisibility(View.INVISIBLE);

                                        }
                                    });

                                } else {
                                    // Handle failures
                                    // ...
                                    String error = task.getException().getMessage();
                                    Toast.makeText(Setup.this, "Image Error" + error, Toast.LENGTH_SHORT).show();

                                    progressBar.setVisibility(View.INVISIBLE);

                                }
                            }
                        });


                        image_path.putFile(mainImageURI).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                if (task.isSuccessful()) {


                                    //storeFireStore();

                                } else {

                                    String error = task.getException().getMessage();
                                    Toast.makeText(Setup.this, "Image Error" + error, Toast.LENGTH_SHORT).show();

                                    progressBar.setVisibility(View.INVISIBLE);

                                }
                            }
                        });

                    }
                }else{


                }
            }
        });

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Account Settings");

        setupImage = findViewById(R.id.profile_image);
        setupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(Setup.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        Toast.makeText(Setup.this, "permissin Denied", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(Setup.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                    } else {
                        //  Toast.makeText(Setup.this,"You already have a Permission",Toast.LENGTH_SHORT).show();

                        BringImagePicker();


                    }
                } else {

                    BringImagePicker();
                }

            }

            private void BringImagePicker() {

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1, 1)

                        .start(Setup.this);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mainImageURI = result.getUri();
                setupImage.setImageURI(mainImageURI);
                isChanged = true;
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }



}
